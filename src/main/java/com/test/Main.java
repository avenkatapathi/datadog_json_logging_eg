package com.test;
// import java.util.logging.Level;
// import java.util.logging.Logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Timer;
import java.util.TimerTask;


public class Main {
    public static void main(String[] args) {
         final Logger logger = LoggerFactory.getLogger(Main.class);
         logger.info("Hello world.");
        //  Logger logger2 = LoggerFactory.getLogger("jsonLogger"); 
        // logger2.debug("Debug message");
       // System.out.println("Hello, World!!!!!!!! :) :) :)");
        // Logger logger= Logger.getLogger(Main.class.getName());
  
        // // log messages using log(Level level, String msg)
        // logger.log(Level.INFO, "hello,This is message 1");
        // logger.log(Level.WARNING, "hello,This is message 2"); 
      //  System.out.println("logs sent");
       
            // do something
            
           
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                  // task to run goes here
                //  System.out.println("print statement with timer !!!");
                  logger.info("info statement.");
                  logger.debug("debug statement");
                  logger.error("error stement");
                  logger.warn("warn statement");
                }
              };
              Timer timer = new Timer();
              long delay = 0;
              long intevalPeriod = 1 * 1000; 
              // schedules the task to be run in an interval 
              timer.scheduleAtFixedRate(task, delay,
                                          intevalPeriod);
    }
}




