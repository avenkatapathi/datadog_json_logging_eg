# Logback JSON example

This project shows how to configure Logback to log in JSON.

Relevant code:

- [`logback.xml`](src/main/resources/logback.xml)
- [`pom.xml`](pom.xml)


Output example:

```java
logger.debug("Hello world.");
```

```json
{
  "timestamp" : "2018-05-26T14:51:07.505Z",
  "level" : "DEBUG",
  "thread" : "main",
  "logger" : "com.test.Main",
  "message" : "Hello world.",
  "context" : "default"
}
```

# The following commands are done to run and deploy the code.

cloning and running :

$ git clone git@gitlab.com:avenkatapathi/datadog_json_logging_eg.git

$ cd datadog_json_logging_eg

[NOTE:Installation of mvn and setup:
Download : https://maven.apache.org/download.cgi

Install : https://maven.apache.org/install.html

https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html


https://stackoverflow.com/questions/45230150/java-home-is-not-working-in-maven
 
 follow the above link and make sure that you add "export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-16.0.2.jdk/Contents/Home"
 in  ~/.mavenrc file and then do,

 $ source  ~/.mavenrc 
 
some more links for reference :https://mkyong.com/java/maven-java_home-is-not-defined-correctly-on-mac-osx/

https://stackoverflow.com/questions/33935281/command-not-found-oh-my-zsh



]

$ mvn clean package 

$ java -cp target/logback-json-example-1.0-SNAPSHOT-jar-with-dependencies.jar com.test.Main


building a docker image and deploying a deployment in kubernetes env:

$ cd ..

$ docker build -t java-hello-world_image:v21 -f datadog_json_logging_eg/Dockerfile .  


[Note: before tagging and pushing the image to a repo make sure that the repo is there or else create one. Go to the following link and create a repo in GCP.
https://console.cloud.google.com/artifacts/browse/mm-shared-services?project=mm-shared-services
Once the repo is created do the "setup instructions" and configure the docker.Copy the url if the repo created using the copy option given there.
Now you can tag and push to the repo you created.
]


$ docker tag java-hello-world_image:v21 us-central1-docker.pkg.dev/mm-shared-services/test-repo/java-hello-world_image:v21     

$ docker push us-central1-docker.pkg.dev/mm-shared-services/test-repo/java-hello-world_image:v21

$ kubectl create namespace test 

$ kubectl apply -f datadog_json_logging_eg/deploy.yml -n test 

checking if the app is successfuly deployed:

$ kubectl get pods -n test 

$ kubectl logs hello-world-ff5bd9f4b-r7b89 -n test 


