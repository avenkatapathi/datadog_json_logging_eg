FROM openjdk:16-alpine3.13
WORKDIR /
ADD datadog_json_logging_eg /
# RUN mkdir logs \
#     # && chown 1000:3000 -R logs \
#     && chmod 777 logs \
#     && ls -al \
#     && echo "current user:" $USER

EXPOSE 8080
ENTRYPOINT java -classpath target/logback-json-example-1.0-SNAPSHOT-jar-with-dependencies.jar com.test.Main
